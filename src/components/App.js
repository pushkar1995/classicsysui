import React from "react";
import { Route, Switch } from "react-router-dom";
import OrderPage from "./OrderPage/OrderPage";
import ClientLoginPage from "./ClientLogin/ClientLoginPage";
import DispatchedOrdersPage from "./OrderPage/DispatchedOrders/DispatchedOrdersPage";
import ProductList from "./ProductList/ProductList";
import Header from "../components/Header_footer/Header/header";
import Footer from "../components/Header_footer/Footer/Footer";
class ReactRouter extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <Switch>
          <Route exact path="/" component={ClientLoginPage} />
          <Route path="/dashboard" component={ProductList} />
          <Route path="/orderPage" component={OrderPage} />
          <Route path="/dashboard" component={ProductList} />
          <Route
            path="/dispatchedOrdersPage"
            component={DispatchedOrdersPage}
          />
          <Route render={() => <h1>Not Found</h1>} />
        </Switch>
        <Footer />
      </React.Fragment>
    );
  }
}

export default ReactRouter;
