import React, { Component } from "react";
import "./ClientLoginFormStyles.css";
import { Redirect } from "react-router-dom";
class ClientLoginForm extends Component {
  state = {
    isLoggedin: false
  };
  handleSubmit = e => {
    e.preventDefault();
    this.setState({ isLoggedin: true });
  };
  render() {
    if (this.state.isLoggedin) {
      return <Redirect to="/dashboard" />;
    }
    return (
      <div className="clientLoginFormContainer">
        <form onSubmit={this.handleSubmit}>
          <p>
            <input
              type="text"
              className="ClientInputFieldStyles"
              // style={[ClientInputFieldStyles]}
              placeholder="Branch Id"
              name="branchId"
              size="40"
            />
          </p>

          <p>
            <input
              type="password"
              className="ClientInputFieldStyles"
              placeholder="Password"
              name="password"
              size="40"
            />
          </p>
          <button className="loginButtonStyles">Login</button>
        </form>
      </div>
    );
  }
}

export default ClientLoginForm;
