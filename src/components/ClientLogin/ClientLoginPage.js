import React, { Component } from "react";
import ClientLoginForm from "./ClientLoginForm";
import "./ClientLoginPageStyles.css";
import classic_Logo from "../../images/classicLogo.png";
class ClientLoginPage extends Component {
  render() {
    return (
      <div className="clientLoginContainer">
        <div className="logoContainer">
          <img src={classic_Logo} alt="classic" className="logo" />
        </div>
        <div className="formContainer">
          <ClientLoginForm />
        </div>
      </div>
    );
  }
}

export default ClientLoginPage;
