import React from "react";
import "./FooterStyles.css"

const Footer = () => {
  return (
    <div className="footerContainer">
      <h4 className="footerText">Copyright all reserved 2019 @ Classic MoMo Pvt. Ltd.</h4>
    </div>
  );
};

export default Footer;
