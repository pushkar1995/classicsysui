import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import classicLogo from "../../../images/classicLogo.jpg";
import "./headerStyles.css";

class Header extends Component {
  active = {
    fontWeight: "bold",
    color: "#ffffff"
    
  };
  render() {
    return (
      <div className="headerContainer">
        <NavLink to="/dashboard" activeStyle={this.active}>
          <img src={classicLogo} className="classicLogoStyles" />
        </NavLink>
        <NavLink
          to="/orderPage"
          className="navLinkStyles"
          activeStyle={this.active}
        >
          ORDERS
        </NavLink>
      </div>
    );
  }
}

export default Header;
