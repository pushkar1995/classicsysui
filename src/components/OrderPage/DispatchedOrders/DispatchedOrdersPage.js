import React, { Component } from 'react';
import DispatchedOrdersTable from './DispatchedOrdersTable';
import './DispatchedOrdersStyles.css'

class DispatchedOrdersPage extends Component {
  render() {
    return (
      <div className="disOrdersContainer">
        <div className="titleContainer">Todays Dispatched Orders From Master Kitchen:</div>
        <div className="disTableContainer"><DispatchedOrdersTable /></div>
        <div className="disApproveButtonContainer">
        <button className="disApproveButtonStyles">Approve</button>
        </div>
      </div>
    );
  }
}

export default DispatchedOrdersPage;
