import React, { Component } from 'react';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

class OrderTable extends Component {
  render() {
      const columns = [
         {
             Header: "Dispatched Order List",
                columns: [
                    {
                        Header: "S.No",
                        accessor: "serialNumber",
                        width: 100,
                        maxWidth: 100,
                        minWidth: 100
                    },
                    {
                        Header: "Name",
                        accessor: "name",
                        // style: {
                        //     textAlign: "right"
                        // }
                        width: 300,
                        maxWidth: 300,
                        minWidth: 300
                    },
                    {
                        Header: "Quantity",
                        accessor: "quantity",
                        width: 100,
                        maxWidth: 100,
                        minWidth: 100
                    },
                ]
         } ,
        
        {
            Header: "Actions",
                columns: [
                    {
                    Header: "Delete",
                    Cell: props => {
                        return(
                            <button style={{backgroundColor: 'maroon'}} >
                                Delete
                            </button>
                        )
                    },
                    sortable: false,
                    filterable: false,
                    width: 100,
                    maxWidth: 100,
                    minWidth: 100
                } 
            ]
        }
    
      ]
    return (
      <div>
        <ReactTable
            columns={columns}
            defaultPageSize={5}
            showPagination
            noDataText={"Please Wait ..."}
            filterable
           
        >

        </ReactTable>
        </div>
    );
  }
}

export default OrderTable;
