import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./OrderPageStyles.css";

class OrderPage extends Component {
  state = {
    orderPageItems: []
  };
  componentDidMount() {
    if (this.props.location.state) {
      return localStorage.setItem(
        "orderItems",
        JSON.stringify(this.props.location.state.orderItems)
      );
    }
    const orderItems = localStorage.getItem("orderItems");

    // console.log(JSON.parse(orderItems));
    this.setState({ orderPageItems: JSON.parse(orderItems) });
    // localStorage.clear("orderItems");
    // localStorage.removeItem("orderItems");
  }
  render() {
    console.log(this.props);
    const { orderPageItems } = this.state;
    return (
      <div className="orderPageContainer">
        <h2>order page</h2>
        <div className="container_fluid">
          <table className="table table-dark">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Handle</th>
              </tr>
            </thead>
            <tbody>
              {orderPageItems &&
                orderPageItems.map(order => (
                  <tr key={order._id}>
                    <th scope="row">{order._id}</th>
                    <td>{order.name}</td>
                    <td>{order.price}</td>
                    <td>{order.unit}</td>
                    <td>{order.unit}</td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default OrderPage;
