import React, { Component } from "react";
import "./ProductList.css";

export class ProductList extends Component {
  state = {
    products: [],
    loading: true,
    orderItems: [],
    count: 0
  };
  componentDidMount() {
    this.fetchProducts();
  }
  fetchProducts = () => {
    fetch("https://9476fc45.ngrok.io/allProduct", { method: "GET" })
      .then(res => res.json())
      .then(
        res =>
          console.log(res) ||
          this.setState({ products: res.products, loading: true })
      );
  };
  handleOrders = orders => {
    this.setState({ count: this.state.count + 1 });
    this.state.orderItems.push(orders);
  };
  gotoOrder = () => {
    const { orderItems } = this.state;
    this.props.history.push({
      pathname: "/orderPage",
      state: { orderItems }
    });
    console.log("go to order");
  };
  render() {
    const { products, orderItems, count } = this.state;
    console.log("order items", orderItems.length);
    return (
      <div className="container-fluid product-list">
        <div className="product-list--title">
          <h4>our all products</h4>
        </div>
        <div className="row">
          <div className="col">
            <button
              type="button"
              className="btn btn-outline-info"
              onClick={() => this.gotoOrder()}
            >
              YOUR ORDERS
              <span className="badge badge-pill badge-danger ml-2">
                {orderItems.length}
              </span>
            </button>
          </div>
        </div>
        <table className="table table-hover table-dark">
          <thead>
            <tr>
              <th scope="col">Product Id</th>
              <th scope="col">Product Name</th>
              <th scope="col">Price</th>
              <th scope="col">Units</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {products &&
              products.map(product => (
                <tr key={product._id}>
                  <th scope="row">{product._id}</th>
                  <td>{product.name}</td>
                  <td>{product.price}</td>
                  <td>{product.unit}</td>
                  <td>
                    <button
                      type="button"
                      className="btn btn-outline-success"
                      onClick={() => this.handleOrders(product)}
                    >
                      ORDER
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ProductList;
